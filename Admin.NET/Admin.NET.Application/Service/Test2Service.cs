using Admin.NET.Application.Const;
using Furion.RemoteRequest.Extensions;
using Magicodes.ExporterAndImporter.Core;
using Magicodes.ExporterAndImporter.Pdf;
using Microsoft.AspNetCore.Authorization;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using Furion.Logging;
using Furion.RemoteRequest;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Security;
using System.Security.Authentication;


namespace Admin.NET.Application.Service;

/// <summary>
/// 自己业务服务
/// </summary>
[ApiDescriptionSettings(ApplicationConst.GroupName, Order = 200)]
[AllowAnonymous]
public class Test2Service : IDynamicApiController, ITransient
{
    public Test2Service()
    {
    }

    /// <summary>
    /// 测试远程请求(框架)
    /// </summary>
    /// <returns></returns>
    [HttpPost("/test1")]
    public async Task Test()
    {
        try
        {

    
        Dictionary<string, string> dataMap = new Dictionary<string, string>();
        dataMap.Add("charset", "111");
        dataMap.Add("version", "111");
        dataMap.Add("signType", "111");
        dataMap.Add("service", "111");
        dataMap.Add("merchantId", "111");
        dataMap.Add("transSerialSn", "111");
        dataMap.Add("cardSn", "111");
        dataMap.Add("entranceNo", "111");
        dataMap.Add("entranceTime", "20220829084004");//data.enterTime
        dataMap.Add("vehplateNo", "京A12345");

        var res = await ("http://127.0.0.1:3003/")
                      .SetBody(dataMap, "application/x-www-form-urlencoded", Encoding.GetEncoding("GBK"))
                      .WithEncodeUrl(false)
                      .PostAsStringAsync();


            Console.WriteLine(res);
        }
        catch(Exception ex)
        {
            Console.WriteLine("___"+ex.Message);
        }
    }

    /// <summary>
    /// 测试远程请求(非框架)
    /// </summary>
    /// <returns></returns>
    [HttpPost("/test2")]
    public async Task Test2()
    {
        try
        {


            Dictionary<string, string> dataMap = new Dictionary<string, string>();
            dataMap.Add("charset", "111");
            dataMap.Add("version", "111");
            dataMap.Add("signType", "111");
            dataMap.Add("service", "111");
            dataMap.Add("merchantId", "111");
            dataMap.Add("transSerialSn", "111");
            dataMap.Add("cardSn", "111");
            dataMap.Add("entranceNo", "111");
            dataMap.Add("entranceTime", "20220829084004");//data.enterTime
            dataMap.Add("vehplateNo", "京A12345");

            String buf = String.Empty;
            GetCertSign( dataMap, ref buf);
            String res1 = Post("http://127.0.0.1:3003/", buf, "GBK");
            Console.WriteLine(res1);
        }
        catch (Exception ex)
        {
            Console.WriteLine("___" + ex.Message);
        }

    }

    [NonAction]
    public static bool GetCertSign( Dictionary<string, string> param, ref string rtnSignData)
    {
        Encoding encoding = Encoding.GetEncoding("GBK");
        string[] vv = new string[param.Count];
        int ii = 0;
        foreach (string key in param.Keys)
        {
            vv[ii++] = key;
        }
        Array.Sort(vv, string.CompareOrdinal);
        for (int i = 0; i < vv.Length; i++)
        {
            string str = "";
            param.TryGetValue(vv[i], out str);
            if (!string.IsNullOrEmpty(str))
            {
                rtnSignData = rtnSignData + vv[i] + "=" + param[vv[i]] + "&";
            }
        }
        if (rtnSignData.Length > 0)
        {
            rtnSignData = rtnSignData.Substring(0, rtnSignData.Length - 1);
            rtnSignData = rtnSignData + "&merchantCert=" + "111" + "&merchantSign=" + "222";
            return true;
        }
        return false;
    }

    [NonAction]
    private static bool CheckValidationResult(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors errors)
    {
        return true; //总是接受  
    }
    [NonAction]
    public static string Post(string url, string parameters, string encodingName)
    {
        string result = "";
        HttpWebRequest req = null;
        try
        {
            //如果是发送HTTPS请求  
            if (url.StartsWith("https", StringComparison.OrdinalIgnoreCase))
            {
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(CheckValidationResult);
                req = WebRequest.Create(url) as HttpWebRequest;
                req.ProtocolVersion = HttpVersion.Version10;
            }
            else
            {
                ServicePointManager.Expect100Continue = false;
                req = WebRequest.Create(url) as HttpWebRequest;
            }
            req.Method = "POST";
            req.ContentType = "application/x-www-form-urlencoded";
            req.Timeout = 1000 * 30;//超时时间：15s
            byte[] data = Encoding.GetEncoding(encodingName).GetBytes(parameters);
            req.ContentLength = data.Length;
            using (Stream reqStream = req.GetRequestStream())
            {
                reqStream.Write(data, 0, data.Length);
                reqStream.Close();
            }

            using (HttpWebResponse resp = (HttpWebResponse)req.GetResponse())
            {
                using (Stream stream = resp.GetResponseStream())
                {
                    //获取响应内容
                    using (StreamReader reader = new StreamReader(stream, Encoding.GetEncoding(encodingName)))
                    {
                        result = reader.ReadToEnd();
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
        finally
        {
            if (req != null)
            {
                req.Abort();
            }
        }
        return result;
    }
}
