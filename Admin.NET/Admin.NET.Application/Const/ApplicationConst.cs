namespace Admin.NET.Application.Const;

/// <summary>
/// 类名说明：辅助代码生成，以项目名.分割的最后一个单词+Const命名
/// </summary>
public class ApplicationConst
{
    /// <summary>
    /// API分组名称
    /// </summary>
    public const string GroupName = "我的业务";

    /// <summary>
    /// 数据库标识
    /// </summary>
    public const string ConfigId = "test";
}